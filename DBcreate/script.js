
var getBody = function() {
  return document.getElementsByTagName('body')[0];
};

var createBlock = function(args) {   
  applyIf(args, {
                  id: '',
                  cls: '',
                  val: '',
                  elem: 'div'
  });

  var block = document.createElement(args.elem);

  if (args.id != null) {
    block.id = args.id;
  }

  if (args.cls != null) {
    block.className = args.cls;
  }

  block.setText = function(txt) {
    if (null != txt && txt.length > 0) {
      this.appendChild(document.createTextNode(txt));
    }
    else {
      if (null != this.firstChild) {
        this.firstChild.remove();
      }
    }
  };

  block.getValue = function() {
    return this.value;
  };

  block.setValue = function(val) {
    this.value = val;
  };

  block.removeBlock = function() {
    this.remove();
  };

  block.setOnClickEvent = function(cbk) {
    if (null != cbk) {
      this.addEventListener("click", cbk);
    }
  };

  block.appendBlock = function(block) {
    if (null != block) {
      this.appendChild(block);
    }
  };

  block.setClass = function(arg) {
    this.className = arg;
  };

  block.setImage = function(img) {
    this.src = img;
  }

  return block;
};


var applyIf = function(args, defaults) {
    for (var key in defaults) {
      if (!args.hasOwnProperty(key) || args[key] == null) {
        args[key] = defaults[key];
      }
    }
};


var doRequest  = function(args, cbk) {
  cbk = cbk || function(data){};
  applyIf(args, {
                method: "GET",
                url: "",  
                label: "",
                texts: ""

  });

  var myJson = JSON.stringify(args.params);

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      cbk(myObj);
    }
   else if (null != args.cbk) 
   {
      args.cbk({});
  }
  };

  xhttp.open(args.method, args.url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send('x='+myJson);

};



var serverAPI = {
                url: {
                    readUsr: 'fromDB.php',
                    writeUsr: 'addData.php',
                    readData: 'dataFromDB.php'
            },
                method: {
                    get: 'GET',
                    post: 'POST'
            }         
};


var getBlock = function(id) {
  return document.getElementById(id);
};


//======================== HTML functions ==============================================


var addData = function() {
  var param = {
    label: getBlock('label').value,
    texts: getBlock('texts').value
  }
  doRequest({url: serverAPI.url.writeUsr, method: serverAPI.method.post, params: param}, function(data){console.log(data)});
  getBlock('label').value = "";
  getBlock('texts').value = "";
};


var takeData = function() {
  doRequest({url: serverAPI.url.readData, method: serverAPI.method.get}, function(data){
      var dataBlock = createBlock({id: 'dataBlock'});
      getBlock('output').appendChild(dataBlock);

    for (obj in data){
      var textsBlock = createBlock({});
      textsBlock.setText(data[obj].txtLabels + " : " + data[obj].texts);
      dataBlock.appendBlock(textsBlock);

    }})
};


var deleteData = function() {
  getBlock('dataBlock').remove();
};

var p;

var getArg = function(arg) {
  p = arg;
  return p;
};

var createImageBlock = function(p) {
  var imageBack = createBlock({cls: 'imageBack'});
  var image = createBlock({elem: 'img', cls: 'winImg'});

  getBody().appendChild(imageBack);
  imageBack.appendBlock(image);
  image.setImage(p);
};




