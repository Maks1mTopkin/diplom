<?php


include 'config.php';
include_once 'lib/Smarty.class.php';
require_once ('Mobile-Detect/Mobile_Detect.php');

$smarty = new Smarty();
$detect = new Mobile_Detect;

$smarty -> assign('siteName', $siteName);

if ( $detect->isMobile() ) {
	$smarty -> display('templatePhone.tpl');
} 
else {
	$smarty -> display('templateComp.tpl');
};


