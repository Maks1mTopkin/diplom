
var domStructure = {
	header: {
		headerTitle: {cls: 'headerTitle'}
	},
	choseBlock: {
		thisBlock: {id: 'choseBlock'},
		choseBlockTextBlock: {
			thisBlock: {cls: 'choseBlockTextBlock'},
			choseBlockTextTitle: {cls: 'choseBlockTextTitle'},
			choseBlockTextText : {cls: 'choseBlockTextText'}
		}
	},
	districtBlock: {
				thisBlock: {cls: 'districtBlock'},
				invitationsDistrict: {elem: 'img', cls: 'invitationsDistrict'},
				videoDistrict: {elem: 'img', cls: 'videoDistrict'},
				webEventDistrict: {elem: 'img', cls: 'webEventDistrict'}
			},

	inWorkBlock: {
		thisBlock: {id: 'inWorkBlock'},
		inWorkBlockText: {cls: 'inWorkBlockText'},
		inWorkBlockBtn: {cls: 'inWorkBlockBtn', elem: 'button'}
	},
	containers: {
		thisBlock: {id: 'containers'},
		containersMenu: {
			thisBlock: {cls: 'containersMenu'},
			containersMenuAboutus: {cls: 'containersMenuAboutus'},
			containersMenuContact: {cls: 'containersMenuContact'}
		},
		containersBlock: {
			thisBlock: {cls: 'containersBlock'},
			weddingContainer: {
				thisBlock: {cls: 'weddingContainer'},
				weddingContainerTitle: {cls: 'weddingContainerTitle'},
				weddingContainerText: {cls: 'weddingContainerText'},
				weddingContainerImage: {cls: 'weddingContainerImage', elem: 'img'}
			},
			adultContainer: {
				thisBlock: {cls: 'adultContainer'},
				adultContainerTitle: {cls: 'adultContainerTitle'},
				adultContainerText: {cls: 'adultContainerText'},
				adultContainerImage: {cls: 'adultContainerImage', elem: 'img'}
			},
			childrenContainer: {
				thisBlock: {cls: 'childrenContainer'},
				childrenContainerTitle: {cls: 'childrenContainerTitle'},
				childrenContainerText: {cls: 'childrenContainerText'},
				childrenContainerImage: {cls: 'childrenContainerImage', elem: 'img'}
			}
		},
			contactBlock: {
				thisBlock: {cls: 'contactBlock'},
				output: {id: 'output'},
				contactBlockTitle: {cls: 'contactBlockTitle'},
				fields: {cls: 'fields'},
				inputFields: {
					thisBlock: {cls: 'inputFields'},
					userName: {elem: 'input', id: 'userName'},
					userEmail: {elem: 'input', id: 'userEmail'},
					subject: {elem: 'input', id: 'subject'}
				},
				inputText: {
					thisBlock: {cls: 'inputText'},
					inputArea: {elem: 'textarea', id: 'inputArea'},
					inputControll: {elem: 'input', id: 'inputControll'}
				},
				inputBtn: {elem: 'button', cls: 'inputBtn'}
				}
		},
		pages: {
			thisBlock: {id: 'pages'},
			pagesMenu: {
				thisBlock: {cls: 'pagesMenu'},
				pagesMenuBack: {cls: 'pagesMenuBack'},
				pagesMenuTitle: {cls: 'pagesMenuTitle'}
			},
			weddingPage: {
				thisBlock: {cls: 'weddingPage'},
				weddingPageImages: {elem: 'img', cls: 'weddingPageImages'}
			},
			adultPage: {
				thisBlock: {cls: 'adultPage'},
				adultPageImages: {elem: 'img', cls: 'adultPageImages'}
			},
			childrenPage: {
				thisBlock: {cls: 'childrenPage'},
				childrenPageImages: {elem: 'img', cls: 'childrenPageImages'}
			}
		}
	}


var weddingID = new Array();
var adultID = new Array();
var childrenID = new Array();

var count = 0;
var messageSendControll = 0;

var serverAPI = {
	url: {
		sendEmail: {url: 'pageBackend/sendEmail.php'},
		labelTexts: {url: 'pageBackend/labelTexts.php'},
		weddingID: {url: 'pageBackend/weddingID.php'},
		adultID: {url: 'pageBackend/adultID.php'},
		childrenID: {url: 'pageBackend/childrenID.php'}
	},
	method: {
		post: 'POST',
		get: 'GET'
	}
};