
var head = function() {
	var headerTitle = createBlock ({cls: domStructure.header.headerTitle.cls});

	headerTitle.setText(getMessage("company.name"));
	getBlock('head').appendChild(headerTitle);

	choseBlockFunc();

	return {
		headerTitle: headerTitle
	}
};


var choseBlockFunc = function () {

	var choseBlock = createBlock({id: domStructure.choseBlock.thisBlock.id});
	var choseBlockTextBlock = createBlock({cls: domStructure.choseBlock.thisBlock.cls});
	var choseBlockTextTitle = createBlock({cls: domStructure.choseBlock.choseBlockTextBlock.choseBlockTextTitle.cls});
	var choseBlockTextText = createBlock({cls: domStructure.choseBlock.choseBlockTextBlock.choseBlockTextText.cls});

	choseBlockTextTitle.setText(getMessage("aboutus"));
	choseBlockTextText.setText(getMessage("aboutus.text"));

	getBlock('mainContainer').appendChild(choseBlock);
	choseBlock.appendBlock(choseBlockTextBlock);
	choseBlockTextBlock.appendBlock(choseBlockTextTitle);
	choseBlockTextBlock.appendBlock(choseBlockTextText);



	var districtBlock = createBlock({cls: domStructure.districtBlock.thisBlock.cls});
	var invitationsDistrict = createBlock({elem: domStructure.districtBlock.invitationsDistrict.elem, cls: domStructure.districtBlock.invitationsDistrict.cls});
	var videoDistrict = createBlock({elem: domStructure.districtBlock.videoDistrict.elem, cls: domStructure.districtBlock.videoDistrict.cls});
	var webEventDistrict = createBlock({elem: domStructure.districtBlock.webEventDistrict.elem, cls: domStructure.districtBlock.webEventDistrict.cls});

	invitationsDistrict.setImage("pic/invitations.jpeg");
	videoDistrict.setImage("pic/video.jpg");
	webEventDistrict.setImage("pic/web.jpg");

	invitationsDistrict.setOnClickEvent(function(){
		choseBlock.removeBlock();
		containers();
	});

	videoDistrict.setOnClickEvent(function(){
		choseBlock.removeBlock();
		inWorkBlockFunc();
	});

	webEventDistrict.setOnClickEvent(function(){
		choseBlock.removeBlock();
		inWorkBlockFunc();
	});

	choseBlock.appendBlock(districtBlock);
	districtBlock.appendBlock(invitationsDistrict);
	districtBlock.appendBlock(videoDistrict);
	districtBlock.appendBlock(webEventDistrict);


};



var inWorkBlockFunc = function() {
	var inWorkBlock = createBlock({id: domStructure.inWorkBlock.thisBlock.id});
	var inWorkBlockText = createBlock({cls: domStructure.inWorkBlock.inWorkBlockText.cls});
	var inWorkBlockBtn = createBlock({cls: domStructure.inWorkBlock.inWorkBlockBtn.cls, elem: domStructure.inWorkBlock.inWorkBlockBtn.elem})

	inWorkBlockText.setText(getMessage("text.empty"));
	inWorkBlockBtn.setText("Back");

	inWorkBlockBtn.setOnClickEvent(function(){
		inWorkBlock.removeBlock();
		choseBlockFunc();
	});

	getBlock('mainContainer').appendChild(inWorkBlock);
	inWorkBlock.appendBlock(inWorkBlockText);
	inWorkBlock.appendBlock(inWorkBlockBtn);
}


var containers = function() {
	// main container creation
	var containers = createBlock({id: domStructure.containers.thisBlock.id});
	getBlock('mainContainer').appendChild(containers);

	// menu block creation
	var containersMenu = createBlock({cls: domStructure.containers.containersMenu.thisBlock.cls});
	var containersMenuAboutus = createBlock({cls: domStructure.containers.containersMenu.containersMenuAboutus.cls});
	var containersMenuContact = createBlock({cls: domStructure.containers.containersMenu.containersMenuContact.cls});

	containersMenuAboutus.setText(getMessage("aboutus"));
	containersMenuContact.setText(getMessage("contact"));

	containersMenuAboutus.setOnClickEvent(function(){
		containers.removeBlock();
		choseBlockFunc();
	});

	containersMenuContact.setOnClickEvent(function(){
		scrollPage(0,10000);
	});

	containers.appendBlock(containersMenu);
	containersMenu.appendBlock(containersMenuAboutus);
	containersMenu.appendBlock(containersMenuContact);

	// blocks creation
	var containersBlock =createBlock({cls: domStructure.containers.containersBlock.thisBlock.cls});

	containers.appendBlock(containersBlock);


	//wedding block creation
	var weddingContainer = createBlock({cls: domStructure.containers.containersBlock.weddingContainer.thisBlock.cls});
	var weddingContainerTitle = createBlock ({cls: domStructure.containers.containersBlock.weddingContainer.weddingContainerTitle.cls});
	var weddingContainerText = createBlock ({cls: domStructure.containers.containersBlock.weddingContainer.weddingContainerText.cls});
	var weddingContainerImage = createBlock ({elem: domStructure.containers.containersBlock.weddingContainer.weddingContainerImage.elem, cls: domStructure.containers.containersBlock.weddingContainer.weddingContainerImage.cls});

	weddingContainerTitle.setText(getMessage("wedding"));
	weddingContainerText.setText(getMessage("wedding.text"));
	weddingContainerImage.setImage("pageBackend/getWedding.php?id="+weddingID[0]);

	weddingContainerImage.setOnClickEvent(function(){
		containers.removeBlock();
		weddingPage();
	});

	containersBlock.appendBlock(weddingContainer);
	weddingContainer.appendBlock(weddingContainerTitle);
	weddingContainer.appendBlock(weddingContainerText);
	weddingContainer.appendBlock(weddingContainerImage);


	//adult Event Block creation
	var adultContainer = createBlock({cls: domStructure.containers.containersBlock.adultContainer.thisBlock.cls});
	var adultContainerTitle = createBlock ({cls: domStructure.containers.containersBlock.adultContainer.adultContainerTitle.cls});
	var adultContainerText = createBlock ({cls: domStructure.containers.containersBlock.adultContainer.adultContainerText.cls});
	var adultContainerImage = createBlock ({elem: domStructure.containers.containersBlock.adultContainer.adultContainerImage.elem, cls: domStructure.containers.containersBlock.adultContainer.adultContainerImage.cls});

	adultContainerTitle.setText(getMessage("adultEvent"));
	adultContainerText.setText(getMessage("adultEvent.text"));
	adultContainerImage.setImage("pageBackend/getAdult.php?id="+weddingID[0]);


	adultContainerImage.setOnClickEvent(function(){
		containers.removeBlock();
		adultPage();
	});

	containersBlock.appendBlock(adultContainer);
	adultContainer.appendBlock(adultContainerTitle);
	adultContainer.appendBlock(adultContainerText);
	adultContainer.appendBlock(adultContainerImage);


	//children Event Block creation
	var childrenContainer = createBlock({cls: domStructure.containers.containersBlock.childrenContainer.thisBlock.cls});
	var childrenContainerTitle = createBlock ({cls: domStructure.containers.containersBlock.childrenContainer.childrenContainerTitle.cls});
	var childrenContainerText = createBlock ({cls: domStructure.containers.containersBlock.childrenContainer.childrenContainerText.cls});
	var childrenContainerImage = createBlock ({elem: domStructure.containers.containersBlock.childrenContainer.childrenContainerImage.elem, cls: domStructure.containers.containersBlock.childrenContainer.childrenContainerImage.cls});

	childrenContainerTitle.setText(getMessage("childrenEvent"));
	childrenContainerText.setText(getMessage("childrenEvent.text"));
	console.log(weddingID[0]);
	childrenContainerImage.setImage("pageBackend/getChildren.php?id="+weddingID[0]);

	childrenContainerImage.setOnClickEvent(function(){
		containers.removeBlock();
		childrenPage();
	});

	containersBlock.appendBlock(childrenContainer);
	childrenContainer.appendBlock(childrenContainerTitle);
	childrenContainer.appendBlock(childrenContainerText);
	childrenContainer.appendBlock(childrenContainerImage);


	// contact block creation
	var contactBlock = createBlock({cls: domStructure.containers.contactBlock.thisBlock.cls});
	var output = createBlock({id: domStructure.containers.contactBlock.output.id});
	var contactBlockTitle = createBlock ({cls: domStructure.containers.contactBlock.contactBlockTitle.cls});
	var contactBlockFields = createBlock({cls: domStructure.containers.contactBlock.fields.cls});
	var contactBlockInputFields = createBlock({cls: domStructure.containers.contactBlock.inputFields.thisBlock.cls});
	var contactBlockUserName = createBlock({elem: domStructure.containers.contactBlock.inputFields.userName.elem, id: domStructure.containers.contactBlock.inputFields.userName.id});
	var contactBlockUserEmail = createBlock({elem: domStructure.containers.contactBlock.inputFields.userEmail.elem, id: domStructure.containers.contactBlock.inputFields.userEmail.id});
	var contactBlockSubject = createBlock({elem: domStructure.containers.contactBlock.inputFields.subject.elem, id: domStructure.containers.contactBlock.inputFields.subject.id});
	var contactBlockInputText = createBlock({cls: domStructure.containers.contactBlock.inputText.thisBlock.cls});
	var contactBlockInputArea = createBlock({elem: domStructure.containers.contactBlock.inputText.inputArea.elem, id: domStructure.containers.contactBlock.inputText.inputArea.id});
	var contactBlockInputControll = createBlock({elem: domStructure.containers.contactBlock.inputText.inputControll.elem, id: domStructure.containers.contactBlock.inputText.inputControll.id});
	var contactBlockButton = createBlock({elem: domStructure.containers.contactBlock.inputBtn.elem, cls: domStructure.containers.contactBlock.inputBtn.cls});


	containers.appendBlock(contactBlock);	
	contactBlock.appendBlock(contactBlockTitle);
	contactBlock.appendBlock(output);
	contactBlock.appendBlock(contactBlockFields);
	contactBlockFields.appendBlock(contactBlockInputFields);
	contactBlockInputFields.appendBlock(contactBlockUserName);
	contactBlockInputFields.appendBlock(contactBlockUserEmail);
	contactBlockInputFields.appendBlock(contactBlockSubject);
	contactBlockFields.appendBlock(contactBlockInputText);
	contactBlockInputText.appendBlock(contactBlockInputArea);
	contactBlockInputText.appendBlock(contactBlockInputControll);
	contactBlock.appendBlock(contactBlockButton);

	contactBlockUserName.value = "";
	contactBlockUserEmail.value = "";
	contactBlockSubject.value = "";
	contactBlockInputArea.value = "";

	contactBlockInputArea.onkeyup = function(){botControll( domStructure.containers.contactBlock.inputText.inputControll.id)};

	contactBlockInputControll.type = "hidden";
	contactBlockInputControll.value = "0";

	contactBlockTitle.setText(getMessage("contact"));
	contactBlockButton.setText(getMessage("send.button"));
	contactBlockUserName.placeholder = getMessage("user.name");
	contactBlockUserEmail.placeholder = getMessage("user.email");
	contactBlockSubject.placeholder = getMessage("subject");
	contactBlockInputArea.placeholder = getMessage("user.text");

	contactBlockButton.setOnClickEvent(function(){
		if(contactBlockUserName.value && contactBlockUserEmail.value && contactBlockSubject.value && contactBlockInputArea.value != null) {
			var args = {
				userName: contactBlockUserName.value,
				userEmail: contactBlockUserEmail.value,
				subject: contactBlockSubject.value,
				text: contactBlockInputArea.value,
				controll: count
			};

			controllFunc({email: contactBlockUserEmail.value, userTexts: args, out: domStructure.containers.contactBlock.output.id});
			if (messageSendControll == 1)   {
				contactBlockUserName.value = "";
				contactBlockUserEmail.value = "";
				contactBlockSubject.value = "";
				contactBlockInputArea.value = "";
			    messageSendControll = 0;
			}
			deleteMessage(domStructure.containers.contactBlock.output.id);
		} else {
			getBlock(domStructure.containers.contactBlock.output.id).innerText = getMessage("empty.fields");
			deleteMessage(domStructure.containers.contactBlock.output.id);
		}
	});


}



var weddingPage = function() {
	var pages = createBlock({id: domStructure.pages.thisBlock.id});

	getBlock('mainContainer').appendChild(pages);

	// create menu
	var pagesMenu = createBlock({cls: domStructure.pages.pagesMenu.thisBlock.cls});
	var pagesMenuBack = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuBack.cls});
	var pagesMenuTitle = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuTitle.cls});

	pagesMenuBack.setOnClickEvent(function(){
		pages.removeBlock();
		containers();
	});

	pagesMenuBack.setText(getMessage("back.button"));
	pagesMenuTitle.setText(getMessage("wedding"));

	pages.appendBlock(pagesMenu);
	pagesMenu.appendBlock(pagesMenuBack);
	pagesMenu.appendBlock(pagesMenuTitle);

	//create page block
	var weddingPage = createBlock({cls: domStructure.pages.weddingPage.thisBlock.cls});

	pages.appendBlock(weddingPage);

	var length = weddingID.length;

	for(i = 0; i < length; i++) {
		var weddingPageImages = createBlock({elem: domStructure.pages.weddingPage.weddingPageImages.elem, cls: domStructure.pages.weddingPage.weddingPageImages.cls});
			weddingPageImages.setImage("pageBackend/getWedding.php?id="+weddingID[i]);
			weddingPage.appendBlock(weddingPageImages)
		};



}


var adultPage = function() {
	var pages = createBlock({id: domStructure.pages.thisBlock.id});

	getBlock('mainContainer').appendChild(pages);

	// create menu
	var pagesMenu = createBlock({cls: domStructure.pages.pagesMenu.thisBlock.cls});
	var pagesMenuBack = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuBack.cls});
	var pagesMenuTitle = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuTitle.cls});

	pagesMenuBack.setOnClickEvent(function(){
		pages.removeBlock();
		containers();
	});

	pagesMenuBack.setText(getMessage("back.button"));
	pagesMenuTitle.setText(getMessage("adultEvent"));

	pages.appendBlock(pagesMenu);
	pagesMenu.appendBlock(pagesMenuBack);
	pagesMenu.appendBlock(pagesMenuTitle);

	//create page block
	var adultPage = createBlock({cls: domStructure.pages.adultPage.thisBlock.cls});

	pages.appendBlock(adultPage);

	var length = adultID.length;

	for(i = 0; i < length; i++) {
		var adultPageImages = createBlock({elem: domStructure.pages.adultPage.adultPageImages.elem, cls: domStructure.pages.adultPage.adultPageImages.cls});
			adultPageImages.setImage("pageBackend/getAdult.php?id="+adultID[i]);
			adultPage.appendBlock(adultPageImages);
		};

};



var childrenPage = function() {
	var pages = createBlock({id: domStructure.pages.thisBlock.id});

	getBlock('mainContainer').appendChild(pages);

	// create menu
	var pagesMenu = createBlock({cls: domStructure.pages.pagesMenu.thisBlock.cls});
	var pagesMenuBack = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuBack.cls});
	var pagesMenuTitle = createBlock({cls: domStructure.pages.pagesMenu.pagesMenuTitle.cls});

	pagesMenuBack.setOnClickEvent(function(){
		pages.removeBlock();
		containers();
	});

	pagesMenuBack.setText(getMessage("back.button"));
	pagesMenuTitle.setText(getMessage("childrenEvent"));

	pages.appendBlock(pagesMenu);
	pagesMenu.appendBlock(pagesMenuBack);
	pagesMenu.appendBlock(pagesMenuTitle);

	//create page block
	var childrenPage = createBlock({cls: domStructure.pages.childrenPage.thisBlock.cls});

	pages.appendBlock(childrenPage);

	var length = childrenID.length;

	for(i = 0; i < length; i++) {
		var childrenPageImages = createBlock({elem: domStructure.pages.childrenPage.childrenPageImages.elem, cls: domStructure.pages.childrenPage.childrenPageImages.cls});
			childrenPageImages.setImage("pageBackend/getChildren.php?id="+childrenID[i]);
			childrenPage.appendBlock(childrenPageImages);
		};

};













