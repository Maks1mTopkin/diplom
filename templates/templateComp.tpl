<!DOCTYPE html>
<html>
<head>
	<title>{$siteName}</title>
	<meta charset="utf-8">
	<script type="text/javascript" src="webResource/init.js"></script>
	<script type="text/javascript" src="webResource/loader.js"></script>
	<link rel="stylesheet" type="text/css" href="style/reset.css">
	<link rel="stylesheet" type="text/css" href="style/pageStyle.css">
	<link rel="stylesheet" type="text/css" href="style/titleDiv.css">
	<link rel="stylesheet" type="text/css" href="style/choseBlock.css">
	<link rel="stylesheet" type="text/css" href="style/container.css">
	<link rel="stylesheet" type="text/css" href="style/containers.css">
</head>
<body onload="init()">
	<div id="header"></div>
	<div id="mainDiv"></div>
	<div id="footer"></div>
</body>
</html>