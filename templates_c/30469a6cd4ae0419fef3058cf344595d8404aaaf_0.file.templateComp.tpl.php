<?php
/* Smarty version 3.1.30, created on 2018-05-23 07:21:43
  from "/Users/Maks1m/sites/Diplom/templates/templateComp.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b04fa674681c2_38595404',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '30469a6cd4ae0419fef3058cf344595d8404aaaf' => 
    array (
      0 => '/Users/Maks1m/sites/Diplom/templates/templateComp.tpl',
      1 => 1526924847,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b04fa674681c2_38595404 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['siteName']->value;?>
</title>
	<meta charset="utf-8">
	<?php echo '<script'; ?>
 type="text/javascript" src="webResource/init.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="webResource/loader.js"><?php echo '</script'; ?>
>
	<link rel="stylesheet" type="text/css" href="style/reset.css">
	<link rel="stylesheet" type="text/css" href="style/pageStyle.css">
	<link rel="stylesheet" type="text/css" href="style/titleDiv.css">
	<link rel="stylesheet" type="text/css" href="style/choseBlock.css">
	<link rel="stylesheet" type="text/css" href="style/container.css">
	<link rel="stylesheet" type="text/css" href="style/containers.css">
</head>
<body onload="init()">
	<div id="header"></div>
	<div id="mainDiv"></div>
	<div id="footer"></div>
</body>
</html><?php }
}
