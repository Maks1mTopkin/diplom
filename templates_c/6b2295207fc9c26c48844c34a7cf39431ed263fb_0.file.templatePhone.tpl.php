<?php
/* Smarty version 3.1.30, created on 2018-04-26 11:01:03
  from "/Users/Maks1m/sites/Diplom/templates/templatePhone.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5ae1954f18d386_05220604',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6b2295207fc9c26c48844c34a7cf39431ed263fb' => 
    array (
      0 => '/Users/Maks1m/sites/Diplom/templates/templatePhone.tpl',
      1 => 1524733254,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ae1954f18d386_05220604 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['siteName']->value;?>
</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/	chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="	sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"><?php echo '</script'; ?>
>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php echo '<script'; ?>
 src="mobileResource/init.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 src="mobileResource/loader.js"><?php echo '</script'; ?>
>
	<link rel="stylesheet" type="text/css" href="styleMobile/choseBlock.css">
	<link rel="stylesheet" type="text/css" href="styleMobile/inWorkBlock.css">
	<link rel="stylesheet" type="text/css" href="styleMobile/containers.css">
	<link rel="stylesheet" type="text/css" href="styleMobile/pages.css">
</head>
<body onload="init()">
<div id="head"></div>
<div id="mainContainer"></div>
<div id="footer"></div>
</body>
</html><?php }
}
