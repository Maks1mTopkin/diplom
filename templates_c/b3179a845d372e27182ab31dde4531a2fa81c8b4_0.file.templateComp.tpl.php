<?php
/* Smarty version 3.1.30, created on 2018-05-31 21:43:21
  from "/Users/Maks1m/Sites/Diplom/templates/templateComp.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_5b105059842f40_97611504',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b3179a845d372e27182ab31dde4531a2fa81c8b4' => 
    array (
      0 => '/Users/Maks1m/Sites/Diplom/templates/templateComp.tpl',
      1 => 1526924847,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b105059842f40_97611504 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $_smarty_tpl->tpl_vars['siteName']->value;?>
</title>
	<meta charset="utf-8">
	<?php echo '<script'; ?>
 type="text/javascript" src="webResource/init.js"><?php echo '</script'; ?>
>
	<?php echo '<script'; ?>
 type="text/javascript" src="webResource/loader.js"><?php echo '</script'; ?>
>
	<link rel="stylesheet" type="text/css" href="style/reset.css">
	<link rel="stylesheet" type="text/css" href="style/pageStyle.css">
	<link rel="stylesheet" type="text/css" href="style/titleDiv.css">
	<link rel="stylesheet" type="text/css" href="style/choseBlock.css">
	<link rel="stylesheet" type="text/css" href="style/container.css">
	<link rel="stylesheet" type="text/css" href="style/containers.css">
</head>
<body onload="init()">
	<div id="header"></div>
	<div id="mainDiv"></div>
	<div id="footer"></div>
</body>
</html><?php }
}
