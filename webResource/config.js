
var domStructure = {
	titleDiv: {id: 'titleDiv'},
	folowBlock: {
		thisBlock: {cls: 'folowBlock', id: 'folowBlock'},
		fbInstBlock: {
			thisBlock: {cls: 'fbInstBlock'},
			fbInstBlockText: {cls: 'fbInstBlockText'},
			fb: {cls: 'fb'},
			instagram: {cls: 'instagram'}
		},
		companyInfoBlock: {cls: 'companyInfoBlock'}
	},
	container: {id: 'container'},
	choseBlock: {
		thisBlock: {id: 'choseBlock'},
		textArea: {cls: 'textArea'},
		textAreaTitle: {cls: 'textAreaTitle'},
		text: {cls: 'text'},
			choseArea: {
				thisBlock: {cls: 'choseArea'},
				invitations: {
					thisBlock: {cls: 'invitations'},
					image: {elem: 'img', cls: 'image'},
					text: {elem: 'span', cls: 'invImgText'}
				},
				video: {
					thisBlock: {cls: 'video'},
					image: {elem: 'img', cls: 'image'},
					text: {elem: 'span', cls: 'videoImgText'}
				},
				web: {
					thisBlock: {cls: 'web'},
					image: {elem: 'img', cls: 'image'},
					text: {elem: 'span', cls: 'webImgText'}
				}
			}
		},
	invitationsBlock: {
		languageSelect: {
			languageSelectBlock: {cls: 'languageSelect'},
			language: {elem: 'a'},
			version: {elem: 'span', cls: 'version'}
		},
		menu: {
			menuHolder: {cls: 'menuHolder'},
			thisBlock: {cls: 'menu'},
			wedding: {elem: 'span', cls: 'wedding'},
			adultEvent: {elem: 'span', cls: 'adultEvent'},
			childrenEvent: {elem: 'span', cls: 'childrenEvent'},
			contact: {elem: 'span', cls: 'contact'},
			aboutus: {elem: 'span', cls: 'aboutus'}
		},
		introduction: {
			thisBlock: {cls: 'introduction'},
				weddingBlock: {
					thisBlock: {cls: 'weddingBlock'},
					weddingBlockHolder: {cls: 'weddingBlockHolder'},
					weddindBlockTitle: {cls: 'weddindBlockTitle'},
					weddindBlockImages: {elem: 'img', cls: 'weddindBlockImages'},
					weddingBlockText: {cls: 'weddingBlockText'}
				},
				adultEventBlock: {
					thisBlock: {cls: 'adultEventBlock'},
					adultEventBlockHolder: {cls: 'adultEventBlockHolder'},
					adultEventBlockTitle: {cls: 'adultEventBlockTitle'},
					adultEventBlockImages: {elem: 'img', cls: 'adultEventBlockImages'},
					adultEventBlockText: {cls: 'adultEventBlockText'}
				},
				childrenEventBlock: {
					thisBlock: {cls: 'childrenEventBlock'},
					childrenEventBlockHolder: {cls: 'childrenEventBlockHolder'},
					childrenEventBlockTitle: {cls: 'childrenEventBlockTitle'},
					childrenEventBlockImages: {elem: 'img', cls: 'childrenEventBlockImages'},
					childrenEventBlockText: {cls: 'childrenEventBlockText'}
				}
			},
			contactBlock: {
				thisBlock: {cls: 'contactBlock'},
				contactBlockHolder: {cls: 'contactBlockHolder'},
				output: {id: 'output'},
				contactBlockTitle: {cls: 'contactBlockTitle'},
				fields: {cls: 'fields'},
				inputFields: {
					thisBlock: {cls: 'inputFields'},
					userName: {elem: 'input', id: 'userName'},
					userEmail: {elem: 'input', id: 'userEmail'},
					subject: {elem: 'input', id: 'subject'}
				},
				inputText: {
					thisBlock: {cls: 'inputText'},
					inputArea: {elem: 'textarea', id: 'inputArea'},
					inputControll: {elem: 'input', id: 'inputControll'}
				},
				inputBtn: {elem: 'button', cls: 'inputBtn'}
			}
		},
		weddingContainer: {
			thisBlock: {id: 'weddingContainer'},
			weddingContainerMenuHolder: {cls: 'weddingContainerMenuHolder'},
			weddingContainerMenu: {cls: 'weddingContainerMenu'},
			weddingContainerMenuBack: {cls: 'weddingContainerMenuBack', elem: 'weddingContainerMenuBack'},
			weddingContainerMenuName: {cls: 'weddingContainerMenuName', elem: 'span'},
			weddingContainerImages: {cls: 'weddingContainerImages'},
			weddingPictures: {cls: 'weddingPictures', elem: 'img'}
		},
		adultEventContainer: {
			thisBlock: {id: 'adultEventContainer'},
			adultEventContainerMenu: {cls: 'adultEventContainerMenu'},
			adultEventContainerMenuHolder: {cls: 'adultEventContainerMenuHolder'},
			adultEventContainerMenuBack: {cls: 'adultEventContainerMenuBack', elem: 'span'},
			adultEventContainerMenuName: {cls: 'adultEventContainerMenuName', elem: 'span'},
			adultEventContainerImages: {cls: 'adultEventContainerImages'},
			adultEventPictures: {cls: 'adultEventPictures', elem: 'img'}
		},
		childrenEventContainer: {
			thisBlock: {id: 'childrenEventContainer'},
			childrenEventContainerMenu: {cls: 'childrenEventContainerMenu'},
			childrenEventContainerMenuHolder: {cls: 'childrenEventContainerMenuHolder'},
			childrenEventContainerMenuBack: {cls: 'childrenEventContainerBack', elem: 'span'},
			childrenEventContainerMenuName: {cls: 'childrenEventContainerName', elem: 'span'},
			childrenEventContainerImages: {cls: 'childrenEventContainerImages'},
			childrenEventPictures: {cls: 'childrenEventPictures', elem: 'img'}
		}
};


var count = 0;
var facebook = 'https://www.facebook.com/2mtcreative/?ref=bookmarks';
var insta = 'https://www.instagram.com/2mtcreative';

var serverAPI = {
	url: {
		sendEmail: {url: 'pageBackend/sendEmail.php'},
		labelTexts: {url: 'pageBackend/labelTexts.php'},
		weddingID: {url: 'pageBackend/weddingID.php'},
		adultID: {url: 'pageBackend/adultID.php'},
		childrenID: {url: 'pageBackend/childrenID.php'}
	},
	method: {
		post: 'POST',
		get: 'GET'
	}
};


var pageVersion = {
	pageVersion: "v 1.1"
};

var languageSelectPage = {
	est: "",
	rus: "index.html"
};


var weddingID = new Array();
var adultID = new Array();
var childrenID = new Array();


var messageSendControll = 0;


