
// function controlls is user email is valid
var controllFunc = function(args) {

  applyIf(args, {
                email: "",
                userTexts: "",
                out: ""
        });

  var regular = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
  var valid = regular.test(args.email);
  if (valid) {output = "";
  doRequest({url: serverAPI.url.sendEmail.url, method: serverAPI.method.post, params: args.userTexts}, function(data){
        getBlock(args.out).innerText = data.answer;
        count = 0;
      });
     messageSendControll = 1;
  }
  else output = getMessage("wrong.email");
  getBlock(args.out).innerText = output;
  return valid;
};


// function deletes answer message in contact block
var deleteMessage = function(arg) {
	var message = getBlock(arg);
		function del () {
			message.innerText = "";
		};
	setTimeout(del, 4000);
};


//function scroll

var scrollPage = function(y1, y2) {
  scroll(y1, y2);
}

// AJAX request function
var doRequest  = function(args, cbk) {
  cbk = cbk || function(data){};
  applyIf(args, {
                method: "GET",
                url: "",  
                params: "",
                userName: "",
                userEmail: "",
                subject: "",
                text: "",
                count: ""

  });

  var myJson = JSON.stringify(args.params);

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      cbk(myObj);
    }
   else if (null != args.cbk) 
   {
      args.cbk({});
  }
  };

  xhttp.open(args.method, args.url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send('x='+myJson);

};


// bot controll function
var botControll = function(arg) {
	getBlock(arg).value = ++count;
  console.log(count);
};


// image block creation
var createImageBlock = function(src) {

  var imageBack = createBlock({cls: 'imageBack', id: 'modal'});
  var imageHolder = createBlock({cls: 'imageHolder'});
  var image = createBlock({elem: 'img', cls: 'winImg'});
  var exitHolder = createBlock({cls: 'exitHolder'});
  var exit = createBlock({cls: 'exit', elem: 'span'});


  getBody().appendChild(imageBack);

  // exit.setText("x");

  exit.setOnClickEvent(function(){removeImage(imageBack)});

  imageBack.appendBlock(imageHolder);
  imageHolder.appendBlock(exitHolder);
  exitHolder.appendBlock(exit);
  imageHolder.appendBlock(image);
  image.setImage(src);
};


var removeImage = function(div) {
  div.remove();
  getMainDiv().className = "";
  getBlock('footer').className = "";
};


var openImage = function(arg) {
    createImageBlock(arg);
};


window.onclick = function(event) {
  modal = getBlock('modal');

  if (event.target == modal) {
    removeImage(modal);
  }
};



var hashControll = function(anchor) {
      if (anchor == "#choseBlock" || anchor == "") {
      start();
    } else if (anchor == "#invitationsBlock") {
      invitationsBlock();
    } else if (anchor == "#weddingBlock") {
      weddingBlockPage()
    } else if (anchor == "#adultEvents") {
      adultEventPage()
    } else if (anchor == "#childEvents") {
      childrenEventPage()
    };
};







