
function main() {
	doRequest({url: serverAPI.url.weddingID.url}, function(data) {
		var length = data.length;

		for (var i = 0; i < length; i++) {
			weddingID.push(data[i].id);
			};
		console.log(weddingID);
	});
	doRequest({url: serverAPI.url.adultID.url}, function(data) {
		var length = data.length;

		for (var i = 0; i < length; i++) {
			adultID.push(data[i].id);
			};
	});
	doRequest({url: serverAPI.url.childrenID.url}, function(data) {
		var length = data.length;

		for (var i = 0; i < length; i++) {
			childrenID.push(data[i].id);
			};
	});

	doRequest({url: serverAPI.url.labelTexts.url}, function(data){bundle = data;
		var anchor = window.location.hash;	
		createTitle();
		hashControll(anchor);
		infoBlock();})
};

function init() {
	var loaded = function(jsLoader) {
		if (jsLoader.filesCount() == 5) {
			main();
		}
	};

	require('webResource/utils.js', loaded);
	require('webResource/view.js', loaded);
	require('webResource/message.js', loaded);
	require('webResource/config.js', loaded);
	require('webResource/functions.js', loaded);
};