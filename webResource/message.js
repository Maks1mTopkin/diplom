
var bundle;

var messageBundle = new function() {

	this.getMessage = function(key) {
  		return null != key ? bundle[key] : '';
	};
	
};

function getMessage(key) {
	return messageBundle.getMessage(key);
}