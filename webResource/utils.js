
var start = function() {
	createChoseBlock();
};


var createTitle = function() {
	var titleDiv = createBlock({id: domStructure.titleDiv.id});
	titleDiv.setText(getMessage("company.name"));

	getBlock('header').appendChild(titleDiv);

};



var infoBlock = function() {

	var folowBlock = createBlock({cls: domStructure.folowBlock.thisBlock.cls, id: domStructure.folowBlock.thisBlock.id});
	var fbInstBlock = createBlock({cls: domStructure.folowBlock.fbInstBlock.thisBlock.cls});
	var fbInstBlockText = createBlock({cls: domStructure.folowBlock.fbInstBlock.fbInstBlockText.cls});
	var fb = createBlock({cls: domStructure.folowBlock.fbInstBlock.fb.cls});
	var instagram = createBlock({cls: domStructure.folowBlock.fbInstBlock.instagram.cls});
	var companyInfoBlock = createBlock({cls: domStructure.folowBlock.companyInfoBlock.cls});
	var version = createBlock({elem: domStructure.invitationsBlock.languageSelect.version.elem, cls: domStructure.invitationsBlock.languageSelect.version.cls});


	getBlock('footer').appendChild(folowBlock);
	folowBlock.appendBlock(fbInstBlock);
	fbInstBlock.appendBlock(fbInstBlockText);
	fbInstBlock.appendBlock(fb);
	fbInstBlock.appendBlock(instagram);
	folowBlock.appendBlock(companyInfoBlock);
	folowBlock.appendBlock(version);


	fb.setOnClickEvent(function(){
							window.open(facebook, '_blank');
						});

	instagram.setOnClickEvent(function(){
									window.open(insta, '_blank');
								});

	fbInstBlockText.setText(getMessage("folow"));
	companyInfoBlock.setText(getMessage("contact.us"));
	fb.setText("f");
	instagram.setText("I");
	version.setText(pageVersion.pageVersion);

};



var createChoseBlock = function() {

	window.location.href = "#choseBlock";

	var mainDiv = getMainDiv();

	//create chose block
	var choseBlock = createBlock({id: domStructure.choseBlock.thisBlock.id});
	var textArea = createBlock({cls: domStructure.choseBlock.textArea.cls});
	var textAreaTitle = createBlock({cls: domStructure.choseBlock.textAreaTitle.cls});
	var text = createBlock({cls: domStructure.choseBlock.text.cls});
	var choseArea = createBlock({cls: domStructure.choseBlock.choseArea.thisBlock.cls});
	var invitations = createBlock({cls: domStructure.choseBlock.choseArea.invitations.thisBlock.cls});
	var invImage = createBlock({elem: domStructure.choseBlock.choseArea.invitations.image.elem, cls: domStructure.choseBlock.choseArea.invitations.image.cls});
	var invText = createBlock({elem: domStructure.choseBlock.choseArea.invitations.text.elem, cls: domStructure.choseBlock.choseArea.invitations.text.cls});
	var video = createBlock({cls: domStructure.choseBlock.choseArea.video.thisBlock.cls});
	var videoImage = createBlock({elem: domStructure.choseBlock.choseArea.video.image.elem, cls: domStructure.choseBlock.choseArea.video.image.cls});
	var videoText = createBlock({elem: domStructure.choseBlock.choseArea.video.text.elem, cls: domStructure.choseBlock.choseArea.video.text.cls});
	var web = createBlock({cls: domStructure.choseBlock.choseArea.web.thisBlock.cls});
	var webImage = createBlock({elem: domStructure.choseBlock.choseArea.web.image.elem, cls: domStructure.choseBlock.choseArea.web.image.cls});
	var webText = createBlock({elem: domStructure.choseBlock.choseArea.web.text.elem, cls: domStructure.choseBlock.choseArea.web.text.cls});


	// append chose block with texts and functions
	textAreaTitle.setText(getMessage("aboutus"));
	text.setText(getMessage("aboutus.text"));
	invText.setText(getMessage("image.text"));
	videoText.setText(getMessage("text.empty"));
	webText.setText(getMessage("text.empty"));
	invImage.setImage('pic/invitations.jpeg');
	videoImage.setImage('pic/video.jpg');
	webImage.setImage('pic/web.jpg');

	invitations.setOnClickEvent(function(){
		var choseBlock = getBlock(domStructure.choseBlock.thisBlock.id);
		choseBlock.removeBlock();
		invitationsBlock();
	});

	mainDiv.appendChild(choseBlock);
	choseBlock.appendBlock(textArea);
	textArea.appendBlock(textAreaTitle);
	textArea.appendBlock(text);
	choseBlock.appendBlock(choseArea);
	choseArea.appendBlock(invitations);
	invitations.appendBlock(invImage);
	invitations.appendBlock(invText);
	choseArea.appendBlock(video);
	video.appendBlock(videoImage);
	video.appendBlock(videoText);
	choseArea.appendBlock(web);
	web.appendBlock(webImage);
	web.appendBlock(webText);
};


var invitationsBlock = function() {

	window.location.href = "#invitationsBlock";

	var mainDiv = getMainDiv();

	//  Menu Block
	var menu = createBlock({cls: domStructure.invitationsBlock.menu.thisBlock.cls});
	var menuHolder = createBlock({cls: domStructure.invitationsBlock.menu.menuHolder.cls});
	var wedding = createBlock({elem: domStructure.invitationsBlock.menu.wedding.elem, cls: domStructure.invitationsBlock.menu.wedding.cls});
	var adultEvent = createBlock({elem: domStructure.invitationsBlock.menu.adultEvent.elem, cls: domStructure.invitationsBlock.menu.adultEvent.cls});
	var childrenEvent = createBlock({elem: domStructure.invitationsBlock.menu.childrenEvent.elem, cls: domStructure.invitationsBlock.menu.childrenEvent.cls});
	var contact = createBlock({elem: domStructure.invitationsBlock.menu.contact.elem, cls: domStructure.invitationsBlock.menu.contact.cls});
	var aboutus = createBlock ({elem: domStructure.invitationsBlock.menu.aboutus.elem, cls: domStructure.invitationsBlock.menu.aboutus.cls})

	// Introduction Block
	var introduction = createBlock({cls: domStructure.invitationsBlock.introduction.thisBlock.cls});

	// Weddind block + 3 pictures in the introduction block
	var weddingBlock = createBlock({cls: domStructure.invitationsBlock.introduction.weddingBlock.thisBlock.cls});
	var weddingBlockHolder = createBlock({cls: domStructure.invitationsBlock.introduction.weddingBlock.weddingBlockHolder.cls});
	var weddindBlockTitle = createBlock({cls: domStructure.invitationsBlock.introduction.weddingBlock.weddindBlockTitle.cls});
	var weddindBlockImage1 = createBlock({elem: domStructure.invitationsBlock.introduction.weddingBlock.weddindBlockImages.elem, cls: domStructure.invitationsBlock.introduction.weddingBlock.weddindBlockImages.cls});
	var weddingBlockText = createBlock({cls: domStructure.invitationsBlock.introduction.weddingBlock.weddingBlockText.cls});

	// Adult Block + 3 pictures in the introduction block
	var adultEventBlock = createBlock({cls: domStructure.invitationsBlock.introduction.adultEventBlock.thisBlock.cls});
	var adultEventBlockHolder = createBlock({cls: domStructure.invitationsBlock.introduction.adultEventBlock.adultEventBlockHolder.cls});
	var adultEventBlockTitle = createBlock({cls: domStructure.invitationsBlock.introduction.adultEventBlock.adultEventBlockTitle.cls});
	var adultEventBlockImage1 = createBlock({elem: domStructure.invitationsBlock.introduction.adultEventBlock.adultEventBlockImages.elem, cls: domStructure.invitationsBlock.introduction.adultEventBlock.adultEventBlockImages.cls});
	var adultEventBlockText = createBlock({cls: domStructure.invitationsBlock.introduction.adultEventBlock.adultEventBlockText.cls});

	// Children block + 3 pictures in the introduction block
	var childrenEventBlock = createBlock({cls: domStructure.invitationsBlock.introduction.childrenEventBlock.thisBlock.cls});
	var childrenEventBlockHolder = createBlock({cls: domStructure.invitationsBlock.introduction.childrenEventBlock.childrenEventBlockHolder.cls});
	var childrenEventBlockTitle = createBlock({cls: domStructure.invitationsBlock.introduction.childrenEventBlock.childrenEventBlockTitle.cls});
	var childrenEventBlockImage1 = createBlock({elem: domStructure.invitationsBlock.introduction.childrenEventBlock.childrenEventBlockImages.elem, cls: domStructure.invitationsBlock.introduction.childrenEventBlock.childrenEventBlockImages.cls});
	var childrenEventBlockText = createBlock({cls: domStructure.invitationsBlock.introduction.childrenEventBlock.childrenEventBlockText.cls});

	// Contact block in the end of page
	var contactBlock = createBlock({cls: domStructure.invitationsBlock.contactBlock.thisBlock.cls});
	var contactBlockHolder = createBlock({cls: domStructure.invitationsBlock.contactBlock.contactBlockHolder.cls});
	var output = createBlock({id: domStructure.invitationsBlock.contactBlock.output.id});
	var contactBlockTitle = createBlock ({cls: domStructure.invitationsBlock.contactBlock.contactBlockTitle.cls});
	var contactBlockFields = createBlock({cls: domStructure.invitationsBlock.contactBlock.fields.cls});
	var contactBlockInputFields = createBlock({cls: domStructure.invitationsBlock.contactBlock.inputFields.thisBlock.cls});
	var contactBlockUserName = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputFields.userName.elem, id: domStructure.invitationsBlock.contactBlock.inputFields.userName.id});
	var contactBlockUserEmail = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputFields.userEmail.elem, id: domStructure.invitationsBlock.contactBlock.inputFields.userEmail.id});
	var contactBlockSubject = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputFields.subject.elem, id: domStructure.invitationsBlock.contactBlock.inputFields.subject.id});
	var contactBlockInputText = createBlock({cls: domStructure.invitationsBlock.contactBlock.inputText.thisBlock.cls});
	var contactBlockInputArea = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputText.inputArea.elem, id: domStructure.invitationsBlock.contactBlock.inputText.inputArea.id});
	var contactBlockInputControll = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputText.inputControll.elem, id: domStructure.invitationsBlock.contactBlock.inputText.inputControll.id});
	var contactBlockButton = createBlock({elem: domStructure.invitationsBlock.contactBlock.inputBtn.elem, cls: domStructure.invitationsBlock.contactBlock.inputBtn.cls});

	// main kontainer
	var container = createBlock({id: domStructure.container.id});


	//append container to mainDiv
	mainDiv.appendChild(container);

	//append menu blok and menu functions + texts
	container.appendBlock(menu);
	menu.appendBlock(menuHolder);
	menuHolder.appendBlock(wedding);
	menuHolder.appendBlock(adultEvent);
	menuHolder.appendBlock(childrenEvent);
	menuHolder.appendBlock(contact);
	menuHolder.appendBlock(aboutus);

	wedding.setText(getMessage("wedding"));
	adultEvent.setText(getMessage("adultEvent"));
	childrenEvent.setText(getMessage("childrenEvent"));
	contact.setText(getMessage("contact"));
	aboutus.setText(getMessage("aboutus"));


	wedding.setOnClickEvent(function(){
		container.removeBlock();
		weddingBlockPage();
	});
	adultEvent.setOnClickEvent(function(){
		container.removeBlock();
		adultEventPage();
	});
	childrenEvent.setOnClickEvent(function(){
		container.removeBlock();
		childrenEventPage();
	});

	contact.setOnClickEvent(function(){
			scrollPage(0,10000);
	});

	aboutus.setOnClickEvent(function(){
		container.removeBlock();
		createChoseBlock();
	});

	//apend wedding block with images and events
	container.appendBlock(introduction);
	introduction.appendBlock(weddingBlock);
	weddingBlock.appendChild(weddingBlockHolder);
	weddingBlockHolder.appendBlock(weddindBlockTitle);
	weddingBlockHolder.appendBlock(weddingBlockText);
	weddingBlockHolder.appendBlock(weddindBlockImage1);

	weddindBlockTitle.setText(getMessage("wedding"));
	weddindBlockImage1.setImage("pageBackend/getWedding.php?id="+weddingID[0]);
	weddingBlockText.setText(getMessage("wedding.text"));
	weddindBlockImage1.setOnClickEvent(function(){
		container.removeBlock();
		weddingBlockPage();
	});


	//append adult block with images and events
	introduction.appendBlock(adultEventBlock);
	adultEventBlock.appendBlock(adultEventBlockHolder);
	adultEventBlockHolder.appendBlock(adultEventBlockTitle);
	adultEventBlockHolder.appendBlock(adultEventBlockText);
	adultEventBlockHolder.appendBlock(adultEventBlockImage1);

	adultEventBlockTitle.setText(getMessage("adultEvent"));
	adultEventBlockImage1.setImage("pageBackend/getAdult.php?id="+weddingID[0]);
	adultEventBlockText.setText(getMessage("adultEvent.text"));
	adultEventBlockImage1.setOnClickEvent(function(){
		container.removeBlock();
		adultEventPage();
	});



	//append children block with images and events
	introduction.appendBlock(childrenEventBlock);
	childrenEventBlock.appendBlock(childrenEventBlockHolder);
	childrenEventBlockHolder.appendBlock(childrenEventBlockTitle);
	childrenEventBlockHolder.appendBlock(childrenEventBlockText);
	childrenEventBlockHolder.appendBlock(childrenEventBlockImage1);

	childrenEventBlockTitle.setText(getMessage("childrenEvent"));
	childrenEventBlockImage1.setImage("pageBackend/getChildren.php?id="+weddingID[0]);
	childrenEventBlockText.setText(getMessage("childrenEvent.text"));
	childrenEventBlockImage1.setOnClickEvent(function(){
		container.removeBlock();
		childrenEventPage();
	});



	//append contact block with events
	container.appendBlock(contactBlock);
	contactBlock.appendBlock(contactBlockHolder);
	contactBlockHolder.appendBlock(contactBlockTitle);
	contactBlockHolder.appendBlock(output);
	contactBlockHolder.appendBlock(contactBlockFields);
	contactBlockFields.appendBlock(contactBlockInputFields);
	contactBlockInputFields.appendBlock(contactBlockUserName);
	contactBlockInputFields.appendBlock(contactBlockUserEmail);
	contactBlockInputFields.appendBlock(contactBlockSubject);
	contactBlockFields.appendBlock(contactBlockInputText);
	contactBlockInputText.appendBlock(contactBlockInputArea);
	contactBlockInputText.appendBlock(contactBlockInputControll);
	contactBlockHolder.appendBlock(contactBlockButton);

	contactBlockUserName.value = "";
	contactBlockUserEmail.value = "";
	contactBlockSubject.value = "";
	contactBlockInputArea.value = "";

	contactBlockInputArea.onkeyup = function(){botControll(domStructure.invitationsBlock.contactBlock.inputText.inputControll.id)};

	contactBlockUserName.tabindex = "1";
	contactBlockUserEmail.tabindex = "2";
	contactBlockSubject.tabindex = "3";
	contactBlockInputArea.tabindex = "4";
	contactBlockButton.tabindex = "5";

	contactBlockInputControll.type = "hidden";
	contactBlockInputControll.value = "0";

	contactBlockTitle.setText(getMessage("contact"));
	contactBlockButton.setText(getMessage("send.button"));
	contactBlockUserName.placeholder = getMessage("user.name");
	contactBlockUserEmail.placeholder = getMessage("user.email");
	contactBlockSubject.placeholder = getMessage("subject");
	contactBlockInputArea.placeholder = getMessage("user.text");

	contactBlockButton.setOnClickEvent(function(){
		if(contactBlockUserName.value && contactBlockUserEmail.value && contactBlockSubject.value && contactBlockInputArea.value != null) {
			var args = {
				userName: contactBlockUserName.value,
				userEmail: contactBlockUserEmail.value,
				subject: contactBlockSubject.value,
				text: contactBlockInputArea.value,
				controll: count
			};

			controllFunc({email: contactBlockUserEmail.value, userTexts: args, out: domStructure.invitationsBlock.contactBlock.output.id});
			if (messageSendControll == 1)   {
				contactBlockUserName.value = "";
				contactBlockUserEmail.value = "";
				contactBlockSubject.value = "";
				contactBlockInputArea.value = "";
			    messageSendControll = 0;
			}
			deleteMessage(domStructure.invitationsBlock.contactBlock.output.id);
		} else {
			getBlock(domStructure.invitationsBlock.contactBlock.output.id).innerText = getMessage("empty.fields");
			deleteMessage(domStructure.invitationsBlock.contactBlock.output.id);
		}
	});
};


var weddingBlockPage = function() {

	window.location.href = "#weddingBlock";

	var mainDiv = getMainDiv();

	var weddingContainer = createBlock({id: domStructure.weddingContainer.thisBlock.id});
	var weddingContainerMenu = createBlock({cls: domStructure.weddingContainer.weddingContainerMenu.cls});
	var weddingContainerMenuHolder = createBlock({cls: domStructure.weddingContainer.weddingContainerMenuHolder.cls});
	var weddingContainerMenuBack = createBlock({cls: domStructure.weddingContainer.weddingContainerMenuBack.cls, elem: domStructure.weddingContainer.weddingContainerMenuBack.elem});
	var weddingContainerMenuName = createBlock({cls: domStructure.weddingContainer.weddingContainerMenuName.cls, elem: domStructure.weddingContainer.weddingContainerMenuName.elem});
	var weddingContainerImages = createBlock({cls: domStructure.weddingContainer.weddingContainerImages.cls});

	weddingContainerMenuBack.setText(getMessage("back.button"));
	weddingContainerMenuName.setText(getMessage("wedding"));

	mainDiv.appendChild(weddingContainer);
	weddingContainer.appendBlock(weddingContainerMenu);
	weddingContainerMenu.appendBlock(weddingContainerMenuHolder);
	weddingContainerMenuHolder.appendBlock(weddingContainerMenuBack);
	weddingContainerMenuHolder.appendBlock(weddingContainerMenuName);
	weddingContainer.appendBlock(weddingContainerImages);


	weddingContainerMenuBack.setOnClickEvent(function(){
		weddingContainer.removeBlock();
		invitationsBlock();
	});

	var length = weddingID.length;

	for(i = 0; i < length; i++) {
		var weddingPictures = createBlock({elem: domStructure.weddingContainer.weddingPictures.elem, cls: domStructure.weddingContainer.weddingPictures.cls});
		weddingContainerImages.appendBlock(weddingPictures);
			weddingPictures.setImage("pageBackend/getWedding.php?id="+weddingID[i]);
			weddingPictures.setOnClickEvent(function(){openImage(this.src)});			
		}
};


var adultEventPage = function() {

	window.location.href = "#adultEvents";

	var mainDiv = getMainDiv();

	var adultEventContainer = createBlock({id: domStructure.adultEventContainer.thisBlock.id});
	var adultEventContainerMenu = createBlock({cls: domStructure.adultEventContainer.adultEventContainerMenu.cls});
	var adultEventContainerMenuHolder = createBlock({cls: domStructure.adultEventContainer.adultEventContainerMenuHolder.cls});
	var adultEventContainerMenuBack = createBlock({cls: domStructure.adultEventContainer.adultEventContainerMenuBack.cls, elem: domStructure.adultEventContainer.adultEventContainerMenuBack.elem});
	var adultEventContainerMenuName = createBlock({cls: domStructure.adultEventContainer.adultEventContainerMenuName.cls, elem: domStructure.adultEventContainer.adultEventContainerMenuName.elem});
	var adultEventContainerImages = createBlock({cls: domStructure.adultEventContainer.adultEventContainerImages.cls});

	adultEventContainerMenuBack.setText(getMessage("back.button"));
	adultEventContainerMenuName.setText(getMessage("adultEvent"));

	mainDiv.appendChild(adultEventContainer);
	adultEventContainer.appendBlock(adultEventContainerMenu);
	adultEventContainerMenu.appendBlock(adultEventContainerMenuHolder);
	adultEventContainerMenuHolder.appendBlock(adultEventContainerMenuBack);
	adultEventContainerMenuHolder.appendBlock(adultEventContainerMenuName);
	adultEventContainer.appendBlock(adultEventContainerImages);


	adultEventContainerMenuBack.setOnClickEvent(function(){
		adultEventContainer.removeBlock();
		invitationsBlock();
	});

	var length = adultID.length;

	for(i = 0; i < length; i++) {
		var adultEventPictures = createBlock({elem: domStructure.adultEventContainer.adultEventPictures.elem, cls: domStructure.adultEventContainer.adultEventPictures.cls});
		adultEventContainerImages.appendBlock(adultEventPictures);
			adultEventPictures.setImage("pageBackend/getAdult.php?id="+adultID[i]);
			adultEventPictures.setOnClickEvent(function(){openImage(this.src)});			
		}

};

var childrenEventPage = function() {

	window.location.href = "#childEvents";

	var mainDiv = getMainDiv();

	var childrenEventContainer = createBlock({id: domStructure.childrenEventContainer.thisBlock.id});
	var childrenEventContainerMenu = createBlock({cls: domStructure.childrenEventContainer.childrenEventContainerMenu.cls});
	var childrenEventContainerMenuHolder = createBlock({cls: domStructure.childrenEventContainer.childrenEventContainerMenuHolder.cls});
	var childrenEventContainerMenuBack = createBlock({cls: domStructure.childrenEventContainer.childrenEventContainerMenuBack.cls, elem: domStructure.childrenEventContainer.childrenEventContainerMenuBack.elem});
	var childrenEventContainerMenuName = createBlock({cls: domStructure.childrenEventContainer.childrenEventContainerMenuName.cls, elem: domStructure.childrenEventContainer.childrenEventContainerMenuName.elem});
	var childrenEventContainerImages = createBlock({cls: domStructure.childrenEventContainer.childrenEventContainerImages.cls});

	childrenEventContainerMenuBack.setText(getMessage("back.button"));
	childrenEventContainerMenuName.setText(getMessage("childrenEvent"));

	mainDiv.appendChild(childrenEventContainer);
	childrenEventContainer.appendBlock(childrenEventContainerMenu);
	childrenEventContainerMenu.appendBlock(childrenEventContainerMenuHolder);
	childrenEventContainerMenuHolder.appendBlock(childrenEventContainerMenuBack);
	childrenEventContainerMenuHolder.appendBlock(childrenEventContainerMenuName);
	childrenEventContainer.appendBlock(childrenEventContainerImages);


	childrenEventContainerMenuBack.setOnClickEvent(function(){
		childrenEventContainer.removeBlock();
		invitationsBlock();
	});

	var length = childrenID.length;

	for(i = 0; i < length; i++) {
		var childrenEventPictures = createBlock({elem: domStructure.childrenEventContainer.childrenEventPictures.elem, cls: domStructure.childrenEventContainer.childrenEventPictures.cls});
		childrenEventContainerImages.appendBlock(childrenEventPictures);
			childrenEventPictures.setImage("pageBackend/getChildren.php?id="+childrenID[i]);
			childrenEventPictures.setOnClickEvent(function(){openImage(this.src)});			
		}

};






