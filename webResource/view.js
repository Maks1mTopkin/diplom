

  if (!('remove' in Element.prototype)){
    Element.prototype.remove = function() {
      if (this.parentNode) {
        this.parentNode.removeChild(this);
      }
    }
  };

var getBody = function() {
  return document.getElementsByTagName('body')[0];
};

var getMainDiv = function() {
  return getBlock('mainDiv');
};

var getBlock = function(id) {
	return null != id ? document.getElementById(id) : null;
};

var createBlock = function(args) {   
  applyIf(args, {
                  id: '',
                  cls: '',
                  val: '',
                  elem: 'div'
  });

  var block = document.createElement(args.elem);

  if (args.id != null) {
    block.id = args.id;
  }

  if (args.cls != null) {
    block.className = args.cls;
  }

  block.setText = function(txt) {
  	if (null != txt && txt.length > 0) {
  		this.appendChild(document.createTextNode(txt));
  	}
  	else {
  		if (null != this.firstChild) {
  			this.firstChild.remove();
  		}
  	}
  };

  block.getValue = function() {
    return this.value;
  };

  block.setValue = function(val) {
    this.value = val;
  };

  block.removeBlock = function() {
    this.remove();
  };

  block.setOnClickEvent = function(cbk) {
    if (null != cbk) {
      this.addEventListener("click", cbk);
    }
  };

  block.appendBlock = function(block) {
    if (null != block) {
      this.appendChild(block);
    }
  };

  block.setClass = function(arg) {
    this.className = arg;
  };

  block.setImage = function(img) {
    this.src = img;
  }

  return block;
};


var applyIf = function(args, defaults) {
    for (var key in defaults) {
      if (!args.hasOwnProperty(key) || args[key] == null) {
        args[key] = defaults[key];
      }
    }
};


var openImage = function(src) {
  var image = document.createElement('img');
  image.src = src;
  window.open(src, image);
}

